import Vue from "vue";
import VueRouter from "vue-router";
import routes from "./routes";
Vue.use(VueRouter);

// configure router
const router = new VueRouter({
  hashbang: false,
  history: true,
  routes, // short for routes: routes
  linkActiveClass: "active"
});

export default router;
