import DashboardLayout from "@/layout/dashboard/DashboardLayout.vue";
// GeneralViews
import NotFound from "@/pages/NotFoundPage.vue";

// Admin pages
import Dashboard from "@/pages/Dashboard.vue";
import Icons from "@/pages/Icons.vue";
import home from "@/pages/home.vue";
import patientpage from "@/pages/PatientPage.vue";
import showappointment from "@/pages/showappointment.vue";
import appointing from "@/pages/appointing.vue";
// import calendar from "@/pages/calendar.vue";
import Redirect from "@/pages/Redirect.vue";

// import home from "./pages/home.vue";
// import showappointment from "./pages/showappointment.vue";
// import appointing from "./pages/appointing.vue";
// import patientpage from "./pages/PatientPage.vue";

const routes = [
  {
    path: "",
    component: DashboardLayout,
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        name: "หน้าแรก",
        component: Dashboard
      },
      {
        path: "icons",
        name: "icons",
        component: Icons
      },
      {
        path: "home",
        name: "home",
        component: home
      },
      {
        path: "patientpage",
        name: "คนไข้",
        component: patientpage
      },
      {
        path: "showappointment",
        name: "ตารางนัด",
        component: showappointment
      },
      {
        path: "appointing",
        name: "การนัดหมาย",
        component: appointing
      },
      // {
      //   path: "calendar",
      //   name: "ปฏิทิน",
      //   component: calendar
      // },
      {
        path: "redirect",
        name: "redirect",
        component: Redirect
      }
    ]
  },
  { path: "*", component: NotFound }
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
