import Vue from "vue";
import Vuex from "vuex";
import { database } from '../components/firebase';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    appointment: [],
    patient: []
  },

  mutations: {
    addappointment(state, { payload }) {
      state.appointment.push(payload);
    },
    deleteinfo(state, { index }) {
      state.appointment.splice(index);
    },
    editinfo(state, { payload }) {
      state.appointment[payload] = {
        date: payload.date,
        time: payload.time,
        patient: payload.patient,
        dentist: payload.dentist,
        work: payload.work,
        note: payload.note
      };
    },
    fetchList(state, res) {
      state.appointment = Object.values(res)
      console.log(state.appointment)
    },
    fetchpatient(state, res) {
      state.patient = Object.values(res)
      console.log(state.patient)
    }
  },

  actions: {
    deleteinfo({ commit }, payload) {
      database.ref("contacts").orderByChild(['.key']).once('value', (snapshot) => {
        database.ref("contacts").child(snapshot.key).remove().then(() => commit("deleteinfo", { payload }));
      })
    },

    // deleteinfo({ commit }, index) {
    //   commit("deleteinfo", { index });
    // },
    editinfo({ commit }, payload) {
      commit("editinfo", { payload });
    },
    fetchList({ commit }) {
      database.ref('contacts').on("value", function (snapshot) {
        commit("fetchList", snapshot.val());
      })
    },
    fetchpatient({ commit }) {
      database.ref('contacts').on("value", function (snapshot) {
        commit("fetchpatient", snapshot.val());
      })
      /*database.ref('contacts').on('valeue', function gotData(data){
        commit(console.log(data));
      })*/
    }
  },

  getters: {
    appointment: state => state.appointment,
    patient: state => state.patient
  }
});