import firebase from 'firebase';

var config = {
    apiKey: "AIzaSyBz1jFbCZ5SdqtWwja5j8i8H4XVhLm3Sc4",
    authDomain: "senior-projex.firebaseapp.com",
    databaseURL: "https://senior-projex.firebaseio.com",
    projectId: "senior-projex",
    storageBucket: "senior-projex.appspot.com",
    messagingSenderId: "242540852758",
    appId: "1:242540852758:web:9436772c49876e6efbe737",
    measurementId: "G-CCB01YSSV1"
}

var app = firebase.initializeApp(config);
export const database = app.database();

// export const databaseRef = firebase();