import Vue from "vue";
import App from "./App.vue";
import router from "./router/index";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import VueAxios from "vue-axios";
import axios from "axios";

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';

import PaperDashboard from "./plugins/paperDashboard";
// import "vue-notifyjs/themes/default.css";

import {store} from './store/store'

Vue.use(PaperDashboard);
Vue.use(VueAxios);
Vue.use(axios);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

/* eslint-disable no-new */
new Vue({
  router,
  store,
  render: h => h(App),
}).$mount("#app");

Vue.component('button-counter', {
  data: function () {
    return {
      count: 0
    }
  },
})



// const routes = [
//   {
//     name: "home",
//     path: "/home",
//     component: home
//   },
//   {
//     name: "appointing",
//     path: "/",
//     component: appointing
//   },
//   {
//     name: "showappointment",
//     path: "/showappointment",
//     component: showappointment
//   },
//   {
//     name: "patientpage",
//     path: '/patientpage',
//     component: patientpage
//   }
// ];

// const router = new VueRouter({ mode: "history", routes: routes });

// Vue.config.productionTip = false;

// new Vue({
//   render: home => home(App),
//   router
// }).$mount("#app");