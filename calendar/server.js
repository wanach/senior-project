var app = require('express')()
var port = process.env.PORT || 7777
var users = require('./users');
const fs = require('fs');
const { google } = require('googleapis');

//import {authorize} from "./calendar"

const Credentials = require('./credentials.json')
const SCOPES = ['https://www.googleapis.com/auth/calendar'];

const TOKEN_PATH = 'token.json';

//var AuthUrl = "https://www.google.com/"

function OAuth2Client(credentials) {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);
    return oAuth2Client;
}

authorize(Credentials)

function authorize(credentials) {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);

    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'online',
        scope: SCOPES,
    });
    return authUrl;
}

function gettoken(code) {
    const oAuth2Client = OAuth2Client(Credentials)
    oAuth2Client.getToken(code, (err, token) => {
        if (err) return console.error('Error retrieving access token', err);
        oAuth2Client.setCredentials(token);
        // Store the token to disk for later program executions
        /*fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
            if (err) return console.error(err);
            console.log('Token stored to', TOKEN_PATH);
        });*/
        //console.log(oAuth2Client)
        //console.log(token);
        SentCalendar(oAuth2Client);
        return oAuth2Client;
    });
}

function SentCalendar(auth) {
    const calendar = google.calendar({ version: 'v3', auth });

    // Create a new event start date instance for temp uses in our calendar.
    const eventStartTime = new Date()
    eventStartTime.setDate(eventStartTime.getDay() + 2)

    // Create a new event end date instance for temp uses in our calendar.
    const eventEndTime = new Date()
    eventEndTime.setDate(eventEndTime.getDay() + 4)
    eventEndTime.setMinutes(eventEndTime.getMinutes() + 45)

    const event = {
        summary: "MFU Dental Clinic Appointment",
        location: "2VJH+H7 Nang Lae, Mueang Chiang Rai District, Chiang Rai",
        description: Work,
        colorId: 1,
        start: {
            dateTime: StartAppointment+":00.439Z",
        },
        end: {
            dateTime: EndAppointment+":00.439Z",
        },
    }

    console.log(StartAppointment+":00.439Z")
    console.log(EndAppointment+":00.439Z")

    calendar.freebusy.query(
        {
            resource: {
                timeMin: eventStartTime,
                timeMax: eventEndTime,
                items: [{ id: 'primary' }],
            },
        },
        (err, res) => {
            // Check for errors in our query and log them if they exist.
            if (err) return console.error('Free Busy Query Error: ', err)

            // Create an array of all events on our calendar during that time.
            const eventArr = res.data.calendars.primary.busy

            // Check if event array is empty which means we are not busy
            if (eventArr.length === 0)
                // If we are not busy create a new calendar event.
                return calendar.events.insert(
                    { calendarId: 'primary', resource: event },
                    err => {
                        // Check for errors and log them if they exist.
                        if (err) return console.error('Error Creating Calender Event:', err)
                        // Else log that the event was created.
                        return console.log('Calendar event successfully created.')
                    }
                )

            // If event array is not empty log that we are busy.
            return console.log(`Sorry I'm busy...`)
        }
    )
}


var bodyParser = require('body-parser')

// parse application/json
app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true
    })
)

app.get('/', function (req, res) {
    res.send('<h1>Hello Node.js</h1>')
})

app.get('/calendar', function (req, res) {
    //var id = req.query.code;
    //
    global.Hn = req.query.hn;
    global.Dates = req.query.date;
    global.Time = req.query.time;
    global.Patient = req.query.patient;
    global.Dentist = req.query.dentist;
    global.Work = req.query.work;
    global.Note = req.query.note;
    global.SplitDate = Dates.split('-');
    global.Day = parseInt(SplitDate[2], 10);

    console.log(Hn);
    console.log(Dates);
    console.log(Time);
    console.log(Patient);
    console.log(Dentist);
    console.log(Work);
    console.log(Note);
    console.log(Day);

    global.StartAppointment = Dates + 'T' + Time;
    console.log(StartAppointment);

    SplitTime = Time.split(':');
    Minute = parseInt(SplitTime[1], 10)
    Hour = (parseInt(SplitTime[0], 10) + 1)
    EndTime = Hour + ':' + Minute

    global.EndAppointment = Dates + 'T' + EndTime;

    console.log(EndAppointment);

    //res.send(' มาแล้ว ')
    if (Hn != '') {
        res.redirect(authorize(Credentials));
    }
})

app.get('/redirect', async function (req, res) {
    var code = req.query.code;
    gettoken(code);
    res.send(code);

    //res.redirect
})

app.listen(port, function () {
    console.log('Starting node.js on port ' + port)
    console.log('Open this link http://localhost:7777/')
    //console.log(authUrl);
})